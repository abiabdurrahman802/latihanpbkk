<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\TravelController;
Use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/biodata', function () {
    return view('biodata');
});

Route::get('/profile021190095', function () {
    return view('profile021190095');
});

Route::get('/profile', [PageController::class, 'profile']);
Route::get('/news', [PageController::class, 'news']);
Route::get('/product', [PageController::class, 'product']);
Route::get('/travel', [PageController::class, 'travel']);
Route::get('/food', [PageController::class, 'food']);


Route::group(['prefix' => '/news'], function () {
    Route::get('/', [NewsController::class, 'index']);
    Route::get('/add', [NewsController::class, 'create']);
    Route::post('/create', [NewsController::class, 'save']);
    Route::get('/delete/{id}', [NewsController::class, 'delete']);
    Route::get('/edit/{id}', [NewsController::class, 'edit']);
    Route::post('/update', [NewsController::class, 'update']);
    Route::get('/detail/{id}', [NewsController::class, 'detail']);

});

Route::group(['prefix' => '/category'], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/add', [CategoryController::class, 'create']);
    Route::post('/create', [CategoryController::class, 'save']);
    Route::get('/delete/{id}', [CategoryController::class, 'delete']);
    Route::get('/edit/{id}', [CategoryController::class, 'edit']);
    Route::post('/update', [CategoryController::class, 'update']);


});

Route::group(['prefix' => '/food'], function () {
    Route::get('/', [FoodController::class, 'index']);
    Route::get('/add', [FoodController::class, 'create']);
    Route::post('/create', [FoodController::class, 'save']);
    Route::get('/delete/{id}', [FoodController::class, 'delete']);
    Route::get('/edit/{id}', [FoodController::class, 'edit']);
    Route::post('/update', [FoodController::class, 'update']);


});

Route::group(['prefix' => '/travel'], function () {
    Route::get('/', [TravelController::class, 'index']);
    Route::get('/add', [TravelController::class, 'create']);

});