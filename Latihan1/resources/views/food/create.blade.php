@extends('layouts.app') 
@section('title','halaman food') 
@section('main') 
 
<div class="container"> 
<div class="row mt-3 mb-3"> 
    <form action="{{url('/food/create/') }}" method="post" enctype="multipart/form-data"> 
        @csrf 
        <div class="mb-3"> 
            <label> Nama </label> 
            <select type="text" class="form-control @if($errors->first('category_id')) 
            is-invalid @endif" name="category_id"
            @foreach (category as $c)
             <option value="{{ $c->id }}">{{$c->name }}</option>
            @endforeach 
            <span class="error invalid-feedback">{{ $errors->first('name') }}</span> 
        </div> 
 
        <div class="mb-3"> 
            <label> Harga </label> 
            <input type="number" class="form-control @if($errors->first('price')) 
            is-invalid @endif" name="price" value="{{ Request::old('price') }}"> 
            <span class="error invalid-feedback">{{ $errors->first('price') }}</span> 
        </div> 
 
        <div class="mb-3"> 
            <label> Foto </label> 
            <input type="file" class="form-control @if($errors->first('foto')) 
            is-invalid @endif" name="foto" value="{{ Request::old('foto') }}"> 
            <span class="error invalid-feedback">{{ $errors->first('foto') }}</span> 
        </div> 
 
        <div class="mb-3"> 
            <label> Deskripsi </label> 
            <textarea name="description" class="form-control"></textarea> 
        </div> 
         
        <div class="mb-3"> 
        <button class="btn btn-primary">SUBMIT</button> 
        </div> 
</div> 
</div> 
 
@endsection