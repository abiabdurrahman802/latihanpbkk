@extends('layouts.app') 
@section('title','halaman food') 
@section('main') 
 
<div class="container"> 
<div class="row mt-3 mb-3"> 
    <a class="btn btn-primary mb-2" href="{{url('food/add') }}">Tambah Data </a> 
    @foreach($data as $food ) 
<div class="col-3 mb-3  "> 
<div class="card"> 
<div class="card-header"> 
    <b>{{ $food->name}}</b> <i>(Rp {{ number_format 
        ($food->price,2,",",".") }})</i> 
</div> 
<div class="card-body"> 
    <img src="{{ $food->image }}" alt="{{ $food->name }}" width="100"> 
    <br><b> 
    {{ $food->description}}</b>
Kategori: {{ $food->category->name }}
</br> 
</div> 
<div class="card-footer"> 
    <a href="{{url('/food/edit/'.$food->id) }}" class="btn btn-warning btn-sm">EDIT</a> 
    <a href="{{url('/food/delete/'.$food->id) }}" class="btn btn-danger btn-sm">HAPUS</a> 
</div> 
</div> 
</div> 
@endforeach 
</div> 
</div> 
 
@endsection