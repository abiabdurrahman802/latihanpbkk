<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>product021190095</title>
</head>
<body>
    <h2>Product Terbaru</h2>
    JAKARTA, KOMPAS.com - Kawasaki ikut memeriahkan Indonesia International Motor Show (IIMS)
    2022 dengan booth seluas 330 m2 di Hall C, JIExpo Kemayoran. Meski tak meluncurkan produk baru,
    Kawasaki berusaha menampilkan berbagai motor-motor unggulan, dari kelas dasar, sedang, menengah,
    sampai premium. Baca juga: Keren! Tuksedo Studio Bali Bikin Mobil Klasik
    Head Sales & Promotion PT. Kawasaki Motor Indonesia, Michael C Tanadhi, mengatakan, meski ajang IIMS 2022 ini sempat tertunda,
    keberadaannya menjadi bagian penting untuk industri otomotif Indonesia. “Kami menilai ajang IIMS 2022 merupakan salah satu ajang yang tepat untuk memperkenalkan
    model motor-motor terbaru kepada masyarakat Indonesia karena menghadirkan beragam lapisan masyarakat Indonesia dengan berbagai kebutuhannya," kata Michael dalam
    keterangan resmi, Sabtu (2/4/2022).

   <pre> <img src="motor.jpg" style="width:500px;height:400px;" alt="Motor Kawasaki"></pre>
</body>
</html>