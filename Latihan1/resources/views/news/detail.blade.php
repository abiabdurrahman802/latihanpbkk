@extends('layouts.app')
@section('title','news')

@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <div class="col-md-10 offset-md-1 mb-3">
            <h2>{{ $news->title }}</h2>
            {{ $news->description }}
        </div>
    </div>
</div>

@endsection