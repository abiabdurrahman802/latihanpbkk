@extends('layouts.app')
@section('title','news')

@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-3" href="{{ url('/news/add') }}
        ">Tambah Data</a>
        @foreach($data as $news)
        <div class="col-3">
        <a class="text-decoration-none text-black" href="{{ url('/news/detail/'.$news->id) }}">
            <div class="card">
                <div class="card-header">
                {{ $news->title }}
                </div>
                <div class="card-body">
                    @if(strlen($news->description) > 100)
                        {{ substr_replace($news->description,"...", 100) }}
                    @endif
                </div>
                <div class="card-footer">
                    <a href="{{ url('/news/edit/'.$news->id) }}" class="btn btn-warning btn-sm">Edit</a>
                  <a href="{{ url('/news/delete/'.$news->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                </div>
            </div>
        </div>
        </a>
        @endforeach
    </div>
</div>

@endsection