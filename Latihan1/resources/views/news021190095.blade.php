<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>news021190095</title>
</head>
<body>
    <h2>Berita Terkini</h2>
    Liputan6.com, Jakarta - Kementerian Perhubungan (Kemenhub) memastikan akan menerapkan skema contra flow dan sistem satu arah atau one way saat arus mudik lebaran. Namun, untuk kepastian waktunya belum dijelaskan secara rinci.
Direktur Jenderal Perhubungan Darat Kemenhub Budi Setiyadi menyampaikan, Kementerian Perhubungan telah dikoordinasikan dengan Korlantas Polri mengenai penerapan kebijakan untuk mendukung kelancaran arus mudik Lebaran 2022.
Terkait contra flow, titik dimana akan diberlakukan dan juga kapan akan dijalankan akan ditentukan lebih lanjut penerapannya.
“Kita dengan Korlantas Polri telah melakukan simulasi bagaimana melakukan contra flow yang baik, dari kilometer mana ke kilometer mana. Dan secara teknis antar dirlantas Polda dengan masing-masing Polda, mulai dari Banten, Metro Jaya, Jawa Barat hingga Jateng, sudah saling koordinasi,” terangnya dalam Media Briefing Kesiapan Angkutan Lebaran 2022, Jumat (8/4/2022).
Ia menyebut, koordinasi dengan sejumlah Polda ini diperlukan. Pasalnya, jika salah satu wilayah menerapkan kebijakan, akan berdampak pada wilayah lainnya, dalam hal ini yang mengatur adalah korps lalu lintas Polda masing-masing daerah.
“Dan contra flow pasti akan kita lakukan di jalan tol,” katanya.
Sementara itu dari sisi penerapan one way atau jalan satu arah, Dirjen Budi menyebut akan menerapkannya. Namun, terkait waktu pelaksanaannya belum putuskan.
“Menyangkut tanggalnya ini sudah kita sepakati, mungkin nanti sifatnya adalah kita rencanakan dari awal. Jadi tanggal dan jamnya, waktunya akan kita tentukan,” katanya.
“Namun kemarin kita masih mendengarkan masukan dari Jasa Marga terkait waktu-waktunya, karena kita akan benchmarking dari yang tahun 2019,” tambah dia.

<pre> <img src="news.png" style="width:500px;height:400px;" alt="Liputan 6"></pre>
</body>
</html>