@extends('layouts.app') 
@section('title','halaman category') 
@section('main') 
 
<div class="container"> 
<div class="row mt-3 mb-3"> 
    <a class="btn btn-primary mb-2" href="{{url('category/add') }}">Tambah Data </a> 
    @foreach($data as $category ) 
<div class="col-3 mb-3  "> 
<div class="card"> 
<div class="card-header"> 
    <b>{{ $category->name}}</b> 
</div> 
<div class="card-body"> 
  {{ $category->name }}
        </div> 
    </div> 
</div> 
@endforeach 
</div> 
</div> 
 
@endsection