@extends('layouts.app')
@section('title','Halaman Travel')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-2" href="{{ url('/travel/add') }}">Tambah Data</a>
        @foreach($data as $food)
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                {{ $food->title }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>