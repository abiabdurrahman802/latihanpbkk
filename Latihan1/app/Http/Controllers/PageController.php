<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function profile(){
        return view('profile021190095');
    }
    function news(){
        return view('news021190095');
    }
    function product(){
        return view('product021190095');
    }
    function travel(){
        return view('travel021190095');
    }
    function food(){
        return view('foods021190095');
    }
}
